/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import javax.swing.JOptionPane;
/**
 * 
 * @author Hyrex
 */

public class db {

	Connection conn = null;

	public static Connection java_db() {

		try {

			Class.forName("org.sqlite.JDBC");
			Connection conn = DriverManager.getConnection("jdbc:sqlite:." + File.separator + "empnet.sqlite");
			// For testing the DB connection uncomment following:
			// JOptionPane.showMessageDialog(null, "Connection to database is successful");

			return conn;

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);
			e.printStackTrace();
			return null;
		}

	}
}
