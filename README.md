# R E A D M E 
===============


## Modul: Implementierungstechniken für Softwareproduktlinien (WS2017/18)
![fin_ovgu](http://www.inf.ovgu.de/skin/vorlage/css/inf/logo_inf_fak.png)
#### (Benduhn, Saake)

Implementing variability using Blackbox-Frameworks with Plug-Ins in the project „Employee Management System“ (EMS)
# Praktische Übung 4


## 1. Source Code and License:

The source code on this project can be found on [Gitlab](https://gitlab.com/yunocare/EMS-Framework).

The project "EMS" is based on an open source project named *"Employee Payroll Management System"* 
published by user *"Hyrex"* (refined the project's state from: 2016-09-30). Source code on this
project can be found [here](https://www.dropbox.com/s/rmiuenk9oeqo3pw/Employee%20Payroll%20System.zip?dl=0),
as well as on Hyrex's [channel](https://www.youtube.com/channel/UC12Z6-QyYjcGmxgaLIxmFwg)

The project itself will be advanced to a Employee Management System with additionally builds in
functionalities for Administration and Report Generation. This project is created for academic 
purpose in the context software product line development using FeatureIDE plugin in Eclipse IDE.
See FeatureIDE's [official Documentation](https://featureide.github.io/) for further information.

**last updated: 2017-12-05**

## 2. Domain Analysis

The variability implementation is based on a [feature model](https://gitlab.com/yunocare/EMS-Antenna-Approach/blob/master/model.xml).
<!---
(adjust the model.xml path) 
-->

![feature model](https://i.imgur.com/HtTg3L3.png)


## 3. Blackbox Framework - Implemented Features:

In the following a short overview will be given on which features are currently implemented or are in the stage of implementation.
<!---
rework implemented features
-->

### 3.1	Menu

The Menu parameter can be set to either „button menu“ or „list menu“. Dependent of the selection, the Main Menu will look differently.

### 3.2	Authentication

There are two ways of authentication implemented. On the one hand side there is a two factor authentication which is the combination of username and password. On the other hand side there is a three factor authentication that will be extended using a specific userrole. So the combination of username, password and role should match a database query.
### 3.3	Theme

A theme can be customized. Setting a specific theme has impact on font color, font size, as well as on the background image. There are three different custom default and one default theme, if no argument is set.
### 3.4	Localization
A localization runtime argument can be set. Dependent on this parameter the langugae will change, if no argument is set, the software profuct will be delivered in english language. Furthermore the localization argument has impact on the date and timestamps as well. In english the date pattern is YYYY-MM-DD, whereas in german language the date is displayed as DD.MM.YYY.
Currently the localization FR (France) and RU (Russian) are in implementation. To run the software using localization:RU argument, an UTF-8 character set is in doing,.
### 3.5	Logging

A log file will be generated to log all activities per session, containing for example all login-approaches, all changes and all user interactions. To store all log files, a seperated directory „logs“ will be created (if not existing) and each log file name is based on a timestamp, to uniquely identify each session. This can make sense in the business case of delivering a cost-free test version and delivering an overall commercial version.

### 3.6	Audit Trail

Additional to logging opportunities, the project line provides an audit trail, in that all changes, sessions and inserts are documented. This functionality is enabled by default. Disabling this functionality is currently in doing. This can make sense in the business case of delivering a cost-free test version and delivering an overall commercial version.

### 3.7	Report Generator (doing)

The ReportGenerator feature should provide different ways to generate reports on the SQLite database, like specific Employee Reports or Reports on total Employee salaries. The report‘s output can either be XML, as well as CSV and PDF. If this feature is selected an additional Button (or list entry, dependent on the MainMenuUI selection) will be available.

## 4.	Implementation preview – FeatureIDE Project

To give a short insight into Framework Implementation from the FeatureIDE Outline point of view:
<!---
(Put some screenshots in here to clarify the project's structure)
 (which interfaces are used for which features....)
-->



## 5.	Running the project

After importing the GIT project to your local environment, you can simply run the programm as a java application.

<!---
(how can be project be run? provide some input here....)
-->
