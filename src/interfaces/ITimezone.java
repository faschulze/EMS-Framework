package interfaces;


public interface ITimezone {
	
	public String currentDateString(); //returns the current date string
	public String currentTimeString(); //returns the current time string


}
